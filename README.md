## Docker container scripts for [LibreHealth Radiology](https://librehealth.io/projects/lh-radiology/)
The repository hosts docker compose and resource files required to build docker 
images and start containers that can be used by users and developers to test the 
latest code in the repository. The Docker container that is created uses standard 
tomcat7:jre8 and mysql:5.7 images. It downloads the official releases the following modules:
 - radiology module (0.1.0-dev)
 - legacyui module (v1.4.0)
 - webservices.rest module (v2.19.0)
 - owa module (v1.8.0)

It initialized database include 2.0 CIEL concepts from dropbox, along with the 
updated data from radiology acceptanceTests

The tomcat:7-jre8 uses OpenJDK 8, which in turn uses Debian 8.1 (Jessie) base 
image. Thus the lh-toolkit is deployed on tomcat7. The MySQL 5.7 image is used 
for the database.

## Running the container
#### Requirements
Please install [Docker](https://docs.docker.com/) and 
[Docker Compose](https://docs.docker.com/compose/install/) to create the 
container images. You will need to pull the container image from the Gitlab Docker 
Container Registry. More details on this will be added later. Ensure that you do 
not have any other servers running on 8080 (tomcat) and 3306 (mysql).

### Pulling the container image
To run containers pull the container image:
```
Coming soon
```
To run the container
````
docker-compose up
````
MySQL will be started first and then lh-toolkit will be started on the containers. 
When you are done using lh-toolkit you can press `Ctrl+C` to stop the container. 

## Using lh-toolkit
To start using lh-toolkit, point your browser to localhost:8080/lh-toolkit . 
The following are the authentication information:

* **User**: admin
* **Pass**: Admin123
